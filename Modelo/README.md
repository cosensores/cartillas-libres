# Ácidos húmicos: principales componentes del humus
### Apuntes para una ciencia comunitaria - Noviembre 2020 
#### Breve reporte borrador y conceptual de prefactibilidad. Contacto: observatoriohumedales@gmail.com y cosensores.pvd@gmail.com

### Definición

Las sustancias húmicas son una parte importante de materia oscura del humus y consisten en mezclas heterogéneas de moléculas de pequeño tamaño que se forman a partir de la transformación bioquímica de células muertas y se asocian mutuamente en estructuras supramoleculares, que pueden separarse en sus componentes de menor tamaño por fraccionamiento químico. Las moléculas húmicas se asocian entre ellas en conformaciones supramoleculares mediante interacciones hidrofóbicas débiles a pH alcalino o neutro y también mediante puentes de hidrógeno a pH bajos.

![foto](/Modelo/Imagenes/1.jpg)

#### Propiedades químicas de las sustancias húmicas (Stevenson 1982).}

Desde fines del siglo XVIII, a las sustancias húmicas se las clasificó como ácidos húmicos, ácidos fúlvicos o huminas. Estas fracciones se definen basándose estrictamente en su solubilidad ya sea en ácido o álcali, describiendo estos materiales operacionalmente y por lo tanto impartiendo poca información química acerca de los materiales extraídos. El término ‘sustancias húmicas‘ se utiliza en un sentido general para distinguirlo los materiales extraídos que son llamados ácidos húmicos y fúlvicos, los cuales son definidos “operacionalmente” basándose en sus solubilidades en soluciones de ácidos o álcalis. Es importante destacar que no existen límites definidos entre los ácidos húmicos, fúlvicos y las huminas. Todos ellos son parte de un sistema supramolecular extremadamente heterogéneo y las diferencias entre estas subdivisiones son debidas a variaciones en la acidez, grado de hidrofobicidad (contenido de restos aromáticos y alquílicos de cadena larga) y la autoasociación de moléculas por efectos entrópicos. Para caracterizar a las sustancias húmicas por su estructura molecular se debe llevar a cabo una separación cromatográfica y/o química de su gran número de moléculas bioorgánicas diferentes.

Algunos de los primeros trabajos en el fraccionamiento de la materia orgánica fueron llevados a cabo por Carl Sprengel y aún hoy forman la base de los métodos en uso. Estos métodos utilizan una solución diluida de hidróxido de sodio (al 2\%) para separar el humus como una dispersión coloidal de los residuos de plantas insolubles en álcali. De esta dispersión, la fracción húmica se precipita por el agregado del ácido, lo que deja un sobrenadante amarillo, la fracción fúlvica. A la porción soluble en alcohol de la fracción húmica generalmente se la llama ácido úlmico. Los ácidos húmicos grises son solubles en medios alcalinos de baja fuerza iónica, los ácidos húmicos pardos son solubles en soluciones alcalinas independientemente de su fuerza iónica, y los ácidos fúlvicos se mantienen en solución independientemente del pH y la fuerza iónica (fuente: [Wikipedia](https://es.wikipedia.org/wiki/%C3%81cido_h%C3%BAmico#:~:text=Los%20%C3%A1cidos%20h%C3%BAmicos%20son%20unos,humus%2C%20materia%20org%C3%A1nica%20del%20suelo.)).

### Formación de sustancias húmicas en el ambiente

* Las sustancias húmicas son la fracción más estable de la materia orgánica de los suelos y puede persistir por miles de años.
* Se originan de la degradación microbiana de biomoléculas de plantas (y posiblemente también de animales), por ejemplo lignina, dispersados en el ambiente luego de la muerte de las células.
* La materia húmica es una estructura supramolecular de moléculas bio-orgánicas de tamaño relativamente pequeño (con una masa molecular <1000) que se autoensambla principalmente mediante interacciones débiles tales como las fuerzas de Van der Waals, π-π, e interacciones CH-π, aparentemente en estructuras de gran tamaño.
* Su color oscuro se debe, parcialmente, a estructuras quinoides y parcialmente a la absorción aumentada de la luz por cromoforos asociados.

![foto](/Modelo/Imagenes/2.jpg)

El río Morichal Largo, un afluente del río Orinoco, típico río de aguas negras debido a los ácidos húmicos de origen vegetal que se encuentran disueltos de manera muy estable y que le dan el característico color oscuro que oscurece el agua en grandes cantidades pero que la hacen transparente a poca profundidad. El color es semejante al de una taza de té, y por la misma razón (fuente: [Wikipedia](https://es.wikipedia.org/wiki/%C3%81cido_h%C3%BAmico#:~:text=Los%20%C3%A1cidos%20h%C3%BAmicos%20son%20unos,humus%2C%20materia%20org%C3%A1nica%20del%20suelo.)).

![foto](/Modelo/Imagenes/3.png)

Ejemplo de un ácido húmico típico, con varios de sus componentes como la quinona, el fenol, el catecol y los azúcares (fuente: [Wikipedia](https://es.wikipedia.org/wiki/%C3%81cido_h%C3%BAmico#:~:text=Los%20%C3%A1cidos%20h%C3%BAmicos%20son%20unos,humus%2C%20materia%20org%C3%A1nica%20del%20suelo.))

### Bibliografía

* Sánchez Sánchez, A. (2002), Mejora de la Eficacia de los Quelatos de Hierro Sintético a Través de Sustancias Húmicas y Aminoácidos (Tesis Doctoral), Departamento de Agroquímica y Bioquímica de la Facultad de Ciencias de la Universidad de Alicante, Alicante, ESP.
\vskip.5cm
* Stevenson, F.J. Humus Chemistry: Genesis, Composition and Reactions. 2nd ed. New York. Wiley Interscience.1994.
\vskip.5cm
* Piccolo, A. Humic Substances in Terrestrial Ecosystems Amsterdam, Elsevier Science B.V. 1996.

* Ussiri, D.A.N.; Johnson, C.E. Characterization of organic matter in a northern hardwood forest soil by 13C NMR spectroscopy and chemical methods. Geoderma. 2003. 111: 123–149.


