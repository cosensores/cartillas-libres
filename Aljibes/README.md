# Perforaciones en el pajonal para abastecimiento de agua (de uso higiénico) en las islas del Bajo Delta del Paraná
### Medida de adaptación al bloom de cianobacterias - Noviembre 2020
#### Breve reporte borrador y conceptual de prefactibilidad. Contacto: observatoriohumedales@gmail.com y cosensores.pvd@gmail.com

### Encuadre de la problemática

* Las/os isleñas/os, en la actualidad y en la mayoría de los casos, bombean agua de los arroyos para cargar los tanques (Figura 1). Esta fuente de agua tiene usos principalmente higiénicos (lavar platos, ropa, ducha, inodoro) y en ocasiones productivos (cuidado de huerta y animales).
* Frente al evento de floración de cianobacterias surge la necesidad de buscar fuentes de agua alternativas de emergencia. La recuperación de saberes de los modos de vida isleños de las generaciones anteriores nos pueden ayudar.
* Perforaciones manuales o mecanizadas en el fondo del jardín/pajonal se están evaluando como medida de adaptación.
* Toda el agua del delta proviene del sistema Paraná-río de La Plata. Un esquema simplificado de conectividad hidrológica nos permite pensar la posibilidad de hallar bajos volúmenes de agua dulce sin cianobacterias en los sedimentos del interior de las islas (Figura 2).

![foto](Aljibes/Imagenes/Fig1.jpg)

Palafito con tanque de agua que tradicionalmente utiliza agua de los canales.

### Implicancias

* La conectividad hidrológica significa que las distintas fuentes de agua están conectadas, por ejemplo, un acuífero (fuente de agua subterránea) está conectado con ríos y lagunas cercanas. 
* Sin embargo, el tiempo de residencia del agua subterránea es mucho mayor que la que se encuentra en el río. Se mueve más lentamente, por lo tanto, el agua que se encuentra en los acuíferos “es más vieja”, no es la misma que se encuentra en los cursos de agua.
* La difusión de bacterias desde los canales al medio poroso del suelo (sedimentos limoarcillosos de la planicie deltaica) probablemente sea lenta o nula y seguramente no sea un ambiente propicio para su expansión.
        
### Desafíos

ETAPA1

* Consultar a los y las pobladores más antiguos, pueden brindarnos saberes muy valiosos.
* Realizar pozos poco profundos (menos de 8m) sobre la parte más alta de la planicie deltaica disponible. 
* Aislar la perforación de eventuales crecidas con sello hidráulico cubriendo la boca de pozo.
* Encamisarlo con algún material para otorgarle estructura a las paredes del pozo.

ETAPA2

* Bombear el agua y registrar cuando tiempo tarda en volver a llenarse para evaluar rendimiento de los pozos.
* Muestrear agua de dicha perforación para análisis microbiológico. 
* Diseñar red de pozos para abastecer conjuntos de casas. 
* Difundir resultados, organizar equipos de perforación.

### Modelo hipotético de conectividad hidrológica en el delta

![foto](Aljibes/Imagenes/Fig2.jpg)
 
Modelo hipotético de conectividad hidrológica en el delta. En condiciones de mareas ordinarias la conectividad de las aguas que bajan del río Paraná es la de los canales principales (C1). Luego, la conectividad disminuye en los canales secundarios (C2), y en las lagunas de centro de isla, excepto en crecidas, y por último la menor conectividad hidrológica es la del agua atrapada en los sedimentos de las islas (C4),  a la cual se puede acceder mediante perforaciones.

### Dos modelos de perforaciones artesanales para mejorar la actuación espontánea de vecinas/nos isleñas/ños
     
![foto](Aljibes/Imagenes/Fig3.png)

Ambos modelos de perforación artesanal evitan construir paredes para proteger la boca de los aljibes como hacían generaciones isleñas previas.

MODELO A
      
* Perforación de 3 a 7 m con pala vizcachera extensible. 
* Encamisado tubo PVC 110 mm ranurado.
* Sello con suelo cemento bien compactado.

MODELO B
  
* Pozo abierto “telescópico” realizado a pala.
* Enterrar un tanque plástico ranurado bien tapado y sellar para evitar que el agua de crecida percole directamente por la superficie.

### Algunas experiencias en curso en 1° y 2° sección

![foto](Aljibes/Imagenes/Fig4.jpg)

Izquierda, nueva perforación tipo aljibe de la vecina Úrsula y el vecino Cristian del arroyo Caraguatá. Derecha, nueva perforación tipo aljibe vecino Ignacio en islas del frente de avance de la segunda sección de islas.

### Limitaciones y consideraciones finales

*  No existen modelos validados de conectividad hidrológica en el delta del río Paraná. Por lo que toda actuación tendrá carácter de experiencia piloto. 
* Las aguas de perforaciones cortas tipo aljibe, sobre todo al inicio y a veces ocasionalmente pueden presentar olor en el agua. No obstante es poco probable que desarrollen floraciones de cianobacterias, pudiendo utilizarse directamente o aplicando tratamientos simples (ej: cloro).
* Deberá evitarse la contaminación cruzada al construir los pozos. Limpiar las herramientas que hayan estado en contacto con agua con cianobacterias y evitar que ingrese agua de crecidas realizando encamisado y sello con cemento o suelo-cemento de todas las tuberías o contenedores que salgan a la superficie. 
* Se evitará realizar perforaciones a menos de 15 m de pozos negros o de efluentes cloacales.
* Este reporte es conceptual y basado en la experiencia científico-técnica y de diálogo de saberes locales del OHD. No reemplaza el rol del Estado y empresas de agua que deben garantizar la distribución de agua.
* Se solicita reportar construcción de nuevos aljibes al OHD y secretarías/direcciones de islas y compartir experiencias exitosas y no exitosas. Cualquier avance deberá monitorearse para asegurar la calidad del agua.
